package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.UserDAO;
import model.UserBean;


/**
 * Servlet implementation class Resister
 */
@WebServlet("/Resister")
public class Resister extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Resister() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		UserBean loginUser=(UserBean)session.getAttribute("loginUser");
		if(loginUser==null) {
			response.sendRedirect("LoginServlet");
		}

		else{RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/resister.jsp");
		dispatcher.forward(request, response);}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		String login_id = request.getParameter("login_id");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String name=request.getParameter("name");
		String birth_date=request.getParameter("birth_date");

		UserDAO dao = new UserDAO();
		String login_id_unique=dao.login_id_unique(login_id);

		if(login_id_unique==null &&
			password1.equals(password2) &&
			!login_id.equals("")&&
			!password1.equals("")&&
			!password2.equals("") &&
			!name.equals("")&&
			!birth_date.equals("")) {
			String password=password1;
			int result=dao.resister(login_id,name,Date.valueOf(birth_date),password);
			if(result>0) {
				response.sendRedirect("UserList");
			}
			else {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("login_id", login_id);
			request.setAttribute("name", name);
			request.setAttribute("birth_date", birth_date);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/resister.jsp");
			dispatcher.forward(request, response);}
		}else {
		request.setAttribute("errMsg", "入力された内容は正しくありません");
		request.setAttribute("login_id", login_id);
		request.setAttribute("name", name);
		request.setAttribute("birth_date", birth_date);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/resister.jsp");
		dispatcher.forward(request, response);}
	}

}
