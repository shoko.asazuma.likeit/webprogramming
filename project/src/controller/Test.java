package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserDAO;
import model.UserBean;

/**
 * Servlet implementation class Test
 */
@WebServlet("/Test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Test() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login_id = "test10";
		String id = "2";
		String password1 = "3434";
		String password2 = "3434";
		String name="しば";
		String birth_date="2017-01-03";
		
		UserDAO dao = new UserDAO();
		
		if(password1.equals(password2) &&
				!password1.equals("")&&
				!password2.equals("") &&
				!name.equals("")&&
				!birth_date.equals("")) 
				{String password=password1;
				int result=dao.renew(id,password,name,Date.valueOf(birth_date));
				if(result>0) {
					response.sendRedirect("UserList");
				}else {
					UserBean user=dao.findById(Integer.parseInt(id));
					request.setAttribute("user",user);
					request.setAttribute("errMsg", "登録に失敗しました");
					request.setAttribute("login_id", login_id);
					request.setAttribute("name", name);
					request.setAttribute("birth_date", birth_date);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/renew.jsp");
					dispatcher.forward(request, response);}
				}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
