package DAO;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.Login;
import model.UserBean;

public class UserDAO {

    public UserBean findByLogin(Login login) {
        Connection conn = null;
        UserBean user=null;
        try {
 
            conn = DAO.getConnection();

            String sql = "SELECT id, login_id, name,birth_date, password, create_date, update_time FROM user where login_id=? and password=? ";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, login.getLoginID());
            String password=code(login.getPass());
            stmt.setString(2, password);
            ResultSet rs = stmt.executeQuery();

            if(!rs.next()) {
            	return null;
            }
            	int id = rs.getInt("id");
            	String login_id = rs.getString("login_id");
            	String name = rs.getString("name");
            	Date birth_date  = rs.getDate("birth_date");
            	
            	Timestamp create_date  = rs.getTimestamp("create_date");
            	Timestamp update_time  = rs.getTimestamp("update_time");

            	user = new UserBean( id, login_id, name,birth_date, password, create_date, update_time);        

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } 
        finally {
        	if (conn != null) {
        		try {
        			conn.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return user;
    }
    
    public UserBean findById(int id) {
        Connection conn = null;
        UserBean user=null;
        try {
 
            conn = DAO.getConnection();

            String sql = "SELECT id, login_id, name,birth_date, password, create_date, update_time FROM user where id=? ";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if(!rs.next()) {
            	return null;
            }
            	id = rs.getInt("id");
            	String login_id = rs.getString("login_id");
            	String name = rs.getString("name");
            	Date birth_date  = rs.getDate("birth_date");
            	String password = rs.getString("password");
            	Timestamp create_date  = rs.getTimestamp("create_date");
            	Timestamp update_time  = rs.getTimestamp("update_time");

            	user = new UserBean( id, login_id, name,birth_date, password, create_date, update_time);        

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } 
        finally {
        	if (conn != null) {
        		try {
        			conn.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return user;
    }
    
    

    public List<UserBean> findAll() {
        Connection conn = null;
        List<UserBean> userList= new ArrayList<UserBean>();
        UserBean user=null;
        try {
            conn = DAO.getConnection();
            // データベースへ接続
        	 //Class.forName("com.mysql.jdbc.Driver");
             //conn = DriverManager.getConnection("jdbc:mysql://localhost/usermanagement", "root","password");


            // SELECT文を準備
            String sql = "SELECT * FROM user where login_id!='admin'";

             // SELECTを実行し、結果表を取得
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())  {
            	int id = rs.getInt("id");
            	String login_id = rs.getString("login_id");
            	String name = rs.getString("name");
            	Date birth_date  = rs.getDate("birth_date");
            	String password = rs.getString("password");
            	Timestamp create_date  = rs.getTimestamp("create_date");
            	Timestamp update_time  = rs.getTimestamp("update_time");

            	user = new UserBean( id, login_id, name,birth_date, password, create_date, update_time);
            	userList.add(user);
            }

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } 
        finally {
        	if (conn != null) {
        		try {
        			conn.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return userList;
    }

    public List<UserBean> findUserList(String login_id,String name,String startDate,String endDate) {
        Connection conn = null;
        List<UserBean> userList= new ArrayList<UserBean>();
        UserBean user=null;
        try {
            // データベースへ接続
        	 //Class.forName("com.mysql.jdbc.Driver");
             //conn = DriverManager.getConnection("jdbc:mysql://localhost/usermanagement", "root","password");

            conn = DAO.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user where login_id!='admin'";

            if(!login_id.equals("")) {
            	sql += " AND login_id = '"+login_id+"'";
            }
            if(!name.equals("")) {
            	sql += " AND name like '%"+name+"%'";
            }
            if(!startDate.equals("")) {
            	sql += " AND birth_date > '"+Date.valueOf(startDate)+"'";	
            }
            if(!endDate.equals("")) {
            	sql += " AND birth_date < '"+Date.valueOf(endDate)+"'";
            }

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next())  {
            	int id = rs.getInt("id");
            	login_id = rs.getString("login_id");
            	name = rs.getString("name");
            	Date birth_date  = rs.getDate("birth_date");
            	String password = rs.getString("password");
            	Timestamp create_date  = rs.getTimestamp("create_date");
            	Timestamp update_time  = rs.getTimestamp("update_time");

            	user = new UserBean( id, login_id, name,birth_date, password, create_date, update_time);

            	userList.add(user);

            }

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } 
        finally {
        	if (conn != null) {
        		try {
        			conn.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return userList;
    }
/*
    public List<UserBean> findByName(String name) {
        Connection conn = null;
        List<UserBean> userList= new ArrayList<UserBean>();
        UserBean user=null;
        try {
            // データベースへ接続
        	 Class.forName("com.mysql.jdbc.Driver");
             conn = DriverManager.getConnection("jdbc:mysql://localhost/usermanagement", "root","password");

            //conn = DAO.getConnection();

            // SELECT文を準備
            String sql = "SELECT id, login_id, name,birth_date, password, create_date, update_time FROM user where name=?";

             // SELECTを実行し、結果表を取得
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);


            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
            	int id = rs.getInt("id");
            	String login_id = rs.getString("login_id");

            	Date birth_date  = rs.getDate("birth_date");
            	String password = rs.getString("password");
            	Timestamp create_date  = rs.getTimestamp("create_date");
            	Timestamp update_time  = rs.getTimestamp("update_time");

            	user = new UserBean( id, login_id, name,birth_date, password, create_date, update_time);
            	userList.add(user);

            }

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } catch (ClassNotFoundException e) {
        	e.printStackTrace();
        }
        finally {
        	if (conn != null) {
        		try {
        			conn.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return userList;
    }
*/
    /*public List<UserBean> findByBirth_date(Date birth_day,Date birth_day2) {
        Connection conn = null;
        List<UserBean> userList= new ArrayList<UserBean>();
        UserBean user=null;
        try {
            // データベースへ接続
        	 Class.forName("com.mysql.jdbc.Driver");
             conn = DriverManager.getConnection("jdbc:mysql://localhost/usermanagement", "root","password");

            //conn = DAO.getConnection();

            // SELECT文を準備
            String sql = "SELECT id, login_id, name,birth_date, password, create_date, update_time FROM user where birth_date between ? and ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDate(1, birth_day);
            stmt.setDate(2, birth_day2);



            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
            	int id = rs.getInt("id");
            	String login_id = rs.getString("login_id");
            	String name = rs.getString("name");
            	Date birth_date  = rs.getDate("birth_date");
            	String password = rs.getString("password");
            	Timestamp create_date  = rs.getTimestamp("create_date");
            	Timestamp update_time  = rs.getTimestamp("update_time");

            	user = new UserBean( id, login_id, name,birth_date, password, create_date, update_time);
            	userList.add(user);

            }

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } catch (ClassNotFoundException e) {
        	e.printStackTrace();
        }
        finally {
        	if (conn != null) {
        		try {
        			conn.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return userList;
    }
    */

    public  int resister(String login_id,String name,Date birth_date,String password) {
        Connection conn = null;
      //  UserBean user=null;
        PreparedStatement st = null;
        int result = 0;
        try {
            // データベースへ接続
        	conn = DAO.getConnection();
        	 
        	 st=conn.prepareStatement("insert into user values (0,?,?,?,?,NOW(),NOW())");

 			st.setString(1, login_id);
 			st.setString(2, name);
 			st.setDate(3, birth_date);
 			password=code(password);
 			st.setString(4, password);


 			result = st.executeUpdate();



	} catch (SQLException e) {
		e.printStackTrace();
		//System.out.println("データの登録に失敗しました。");
	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
    }



    public String login_id_unique(String login_id) {
        Connection conn = null;
        String name=null;

        try {
            // データベースへ接続
        	conn = DAO.getConnection();

            // SELECT文を準備
            String sql = "SELECT id, login_id, name,birth_date, password, create_date, update_time FROM user where login_id=?";

             // SELECTを実行し、結果表を取得
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, login_id);

            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {

            	name = rs.getString("name");



            }

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        
        }
        finally {
        	if (conn != null) {
        		try {
        			conn.close();
        		} catch (SQLException e) {
        			e.printStackTrace();
        			return null;
        		}
        	}
        }
        return name;
    }

    public  int delete(String id) {
        Connection conn = null;
      //  UserBean user=null;
        PreparedStatement st = null;
        int result = 0;
        try {
            // データベースへ接続
        	conn = DAO.getConnection();

        	 st=conn.prepareStatement("DELETE FROM user WHERE id = ?");
 			st.setString(1, id);
 
 			result = st.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
    }

    public  int renew(String id,String password,String name,Date birth_date) {
        Connection conn = null;
      //  UserBean user=null;
        PreparedStatement st = null;
        int result = 0;
        try {
            // データベースへ接続
        	conn = DAO.getConnection();
        	 
        	 st=conn.prepareStatement("UPDATE user SET  password= ?,name=?,birth_date=?, update_time=now() WHERE id = ?");
        	  password=code(password);
              st.setString(1, password);
              st.setString(2, name);
              st.setDate(3, birth_date);
              st.setString(4, id);
 
 			result = st.executeUpdate();

	} catch (SQLException  e) {
		e.printStackTrace();
		//System.out.println("データの登録に失敗しました。");
	} finally {
		try {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        	return result;
    }



public  String code(String password) {
	//ハッシュを生成したい元の文字列
	String source = password;
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	//ハッシュ生成処理
	byte[] bytes = null;
	try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	} catch (NoSuchAlgorithmException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	}
	String result = DatatypeConverter.printHexBinary(bytes);
	//標準出力

	return result;
	
}

}