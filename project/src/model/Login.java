package model;

public class Login {
	private String loginID;
	private String pass;
	
	public Login(String loginID,String pass){
		this.loginID= loginID;
		this.pass= pass;
	}

	public String getLoginID() {
		return loginID;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	
 
}
