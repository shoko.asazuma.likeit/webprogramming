package model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class UserBean //implements java.io.Serializable
    {
	private int id;
	private String login_id;
	private String name;
	private Date birth_date;
	private String password;
	private Timestamp create_date;
	private Timestamp update_time;

	public UserBean(int id,String login_id, String name, Date birth_date,String password, Timestamp create_date,Timestamp update_time) {
		this.id = id;
		this.login_id = login_id;
		this.name = name;
		this.birth_date = birth_date;
		this.password = password;
		this.create_date = create_date;
		this.update_time = update_time;

	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirth_date() {
		return birth_date;
	}
	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Timestamp getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Timestamp create_date) {
		this.create_date = create_date;
	}
	public Timestamp getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(Timestamp update_time) {
		this.update_time = update_time;
	}
	public String getFormatedBirth_date() {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy年MM月dd日");
		String formatedDate = simpledateformat.format(this.birth_date);

		return formatedDate;
	}

	public String getFormatedUpdate_time() {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
		String formatedDate = simpledateformat.format(this.update_time);

		return formatedDate;
	}
	public String getFormatedCreate_date() {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
		String formatedDate = simpledateformat.format(this.create_date);

		return formatedDate;
	}

	}




