package model;

import DAO.UserDAO;

public class LoginLogic {
	public boolean execute(Login login) {
		UserDAO dao = new UserDAO();
		
		UserBean result=dao.findByLogin(login);
		return result != null;
	}

}
