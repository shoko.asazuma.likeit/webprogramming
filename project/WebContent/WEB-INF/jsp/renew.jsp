<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
<title>ユーザ情報更新</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/original/common.css" rel="stylesheet">
</head>
<body>
    <header>
      <nav class="navbar navbar-inverse navbar-light bg-light">
      	<div class="container">
      		<div class="navbar-header">
            <a class="navbar-brand" href="UserList">ユーザ管理システム</a>
      		</div>

          <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${loginUser.name}さん </li>
  			<li>
  			  <a href="Logout">ログアウト</a>
            </li>
  		  </ul>
      	</div>
      </nav>
    </header>
<h1 class="text-center">ユーザ情報更新</h1>
	<c:if test="${errMsg != null}" >
		<p class="text-danger"> ${errMsg}</p>
	</c:if>
ログインID:${user.login_id}${login_id}
<form action="Renew" method="post">
<input type="hidden" value="${user.login_id}${login_id}" name="login_id">
<input type="hidden" value="${user.id}${id}" name="id">
パスワード<input type="text" name="password1"><br>
パスワード（確認）<input type="text" name="password2"><br>
ユーザー名<input type="text" name="name" value="${user.name}${name}"><br>
生年月日<input type="date" name="birth_date" value="${user.birth_date}${birth_date}"><br>
<button type="submit" value="更新" class="btn btn-info btn-sm">更新</button>
</form>



<a href="UserList">戻る</a>


</body>
</html>