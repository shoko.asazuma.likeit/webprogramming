<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
<title>ユーザ情報詳細</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/original/common.css" rel="stylesheet">

</head>
<body>
<header>
      <nav class="navbar navbar-inverse navbar-light bg-light">
      	<div class="container">
      		<div class="navbar-header">
            <a class="navbar-brand" href="UserList">ユーザ管理システム</a>
      		</div>

          <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${loginUser.name}さん </li>
  			<li>
  			  <a href="Logout">ログアウト</a>
            </li>
  		  </ul>
      	</div>
      </nav>
    </header>
<h1 class="text-center">ユーザ情報詳細参照</h1>
<p>ログインID:${user.login_id}</p>
<p>ユーザ名:${user.name}</p>
<p>生年月日:${user.formatedBirth_date}</p>
<p>登録日時:${user.formatedCreate_date}</p>
<p>更新日時:${user.formatedUpdate_time}</p>


<a href="UserList" class="btn btn-info btn-sm">戻る</a>
</body>
</html>