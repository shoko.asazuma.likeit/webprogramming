<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
<title>ユーザ削除</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/original/common.css" rel="stylesheet">
</head>
<body>
<header>
      <nav class="navbar navbar-inverse navbar-light bg-light">
      	<div class="container">
      		<div class="navbar-header">
            <a class="navbar-brand" href="UserList">ユーザ管理システム</a>
      		</div>

          <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${loginUser.name}さん </li>
  			<li>
  			  <a href="Logout">ログアウト</a>
            </li>
  		  </ul>
      	</div>
      </nav>
    </header>
ログインID:${user.login_id}<br>
を本当に削除してもよろしいでしょうか。
<form action="Delete" method="post">
<input type="hidden" value="${user.id}" name="id">
<button type="submit" value="OK" class="btn btn-info btn-sm">OK</button>
<a href="UserList" class="btn btn-info btn-sm">キャンセル</a>
</form>
</body>
</html>