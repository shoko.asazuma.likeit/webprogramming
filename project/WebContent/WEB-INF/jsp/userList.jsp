<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="model.UserBean"%>
<%@ page import="DAO.UserDAO"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
<title>ユーザ一覧</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/original/common.css" rel="stylesheet">
</head>
<body>

    <header>
      <nav class="navbar navbar-inverse navbar-light bg-light">
      	<div class="container">
      		<div class="navbar-header">
            <a class="navbar-brand" href="UserList">ユーザ管理システム</a>
      		</div>

          <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${loginUser.name}さん </li>
  			<li>
  			  <a href="Logout">ログアウト</a>
            </li>
  		  </ul>
      	</div>
      </nav>
    </header>

<h1 class="text-center">ユーザ一覧</h1>
<c:if test="${loginUser.id==1}"><a href="Resister">新規登録</a></c:if>
	<form action="UserList" method="post">
		<p>ログインID<input type="text" name="login_id"></p>
		<p>ユーザ名<input type="text" name="name"></p>
		<p>生年月日<input type="date" name="birth_date">〜<input type="date" name="birth_date2"></p>
		<button type="submit" value="検索" class="btn btn-info btn-sm">検索</button>
	</form>

	<table class="user_list" border=1>
		<tr>
			<th>ログインID</th>
			<th>ユーザー名</th>
			<th>生年月日</th>
			<th></th>

		</tr>
		<c:forEach var="user" items="${userList}">
			<tr>
				<td>${user.login_id}</td>
				<td>${user.name}</td>
				<td>${user.formatedBirth_date}</td>
				<td>
				<a href="Detail?id=${user.id}">参照</a>
				<%--
				<form action="Detail"  method="post">
				<input type="hidden" value="${user.login_id}" name="login_id">
				<input type="hidden" value="${user.password}" name="password">
				<input type="submit" value="詳細">
				</form>
				--%>
				<c:if test="${user.id==loginUser.id || loginUser.id==1}">
				<a href="Renew?id=${user.id}">更新</a>
				<%--
				<form action="Renew"  method="post">
				<input type="hidden" value="${user.login_id}" name="login_id">
				<input type="hidden" value="${user.password}" name="password">
				<input type="submit" value="更新">
				</form>
				--%>
				</c:if>
				<c:if test="${loginUser.id==1}">
				<a href="Delete?id=${user.id}">削除</a>

				<%--
				<form action="Delete"  method="post">
				<input type="hidden" value="${user.login_id}" name="login_id">
				<input type="hidden" value="${user.password}" name="password">
				<input type="submit" value="削除">
				</form>
				--%>

				</c:if>
				</td>
				</tr>
		</c:forEach>
	</table>

</body>
</html>