<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset=UTF-8>
<title>ログイン画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/original/common.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="UserList">ユーザ管理システム</a>

 </nav>
<div class="text-center">
<h1 class="font-weight-normal display-5 text-dark">ログイン画面</h1>
<form action="LoginServlet" method="post">
<p class="text-dark">ログインID<input type="text" name="ID"></p>
<p class="text-dark">パスワード<input type="password" name="pass"></p>
<input type="submit" value="ログイン" class="btn btn-primary btm-sm">
</form>
	<c:if test="${errMsg != null}" >
		<p class="text-danger"> ${errMsg}</p>
	</c:if>
</div>
</body>
</html>